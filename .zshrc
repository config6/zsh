# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
setopt HIST_IGNORE_DUPS
setopt appendhistory autocd
bindkey -v
autoload -Uz compinit
compinit
# End of lines added by compinstall

source ${ZDOTDIR}/alias.ini


clear_color=$'%{\x1b[0m%}'


function prompt {
        PROMPT=""

        if [[ $PWD == "/" ]]; then
                PROMPT=$'%{\033[01;34m%}/%{\033[00m%}'
        else
                PROMPT=$'%{\033[01;34m%}'${PWD##*/}$'%{\033[00m%}'
        fi

        PROMPT+=' > '

	if [[ $nextexe < $(date +%s) || $PWD != $lastpwd ]]; then
		RPROMPT=$(python ~/.config/zsh/prompt.py)
		lastpwd=$PWD
	else
		RPROMPT=""
	fi
	nextexe=$(( $(date +%s) + 1 ))
}
precmd_functions+=(prompt)

