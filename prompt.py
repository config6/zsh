import subprocess


def no_space(string):
    return "%{" + string + "%}"

clear_color = no_space("\x1b[0m")

def color(r, g, b, string):
    color_string = no_space("\x1b[38;2;{};{};{}m".format(r, g, b))
    return color_string + string + clear_color

def red(string):
    return color(255, 103, 103, string)

def green(string):
    return color(0, 255, 0, string)

def blue(string):
    return color(0, 0, 255, string)

def yellow(string):
    return color(255, 255, 0, string)

def get_git_file_infos(gitfile):
    stage = gitfile[1] == " "

    if stage:
        status = gitfile[0]
    else:
        status = gitfile[1]

    name = gitfile[3:]

    return stage, status, name



git_short_status = subprocess.getoutput("git status -s")
git_branch = subprocess.getoutput("git rev-parse --abbrev-ref HEAD")

if git_short_status[:5] == "fatal":
    exit(1)

stage_files = []
flags = ""

if git_short_status:
    for gitfile in git_short_status.split("\n"):
        stage, status, name = get_git_file_infos(gitfile)

        if stage:
            stage_files.append(name)
        else:
            flags += status

rprompt = "< "
if git_short_status == "":
    rprompt += green(git_branch)
else:
    rprompt += red(git_branch)

if stage_files:
    rprompt += " ("
    for f in stage_files[:-1]:
        rprompt += yellow(f) + ", "
    rprompt += yellow(stage_files[-1]) + ")"

if 0 < len(flags) < 10:
    rprompt += " " + red(flags)

print(rprompt)
